export const darkTheme = {
    background: '#303030',
    accent: '#92BDA3',
    accentTextColor: '#303030',
    textColor: 'white'
}
 
export const lightTheme = {
    background: 'white',
    accent: 'rgba(88,14,26,1)',
    accentTextColor: 'white',
    textColor: 'black'
}