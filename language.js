export const hu = {
    settings: 'Beállítások',
    language: 'Nyelv',
    theme: 'Sötét mód',
    madeby: 'Készítette:',
    originalNews: 'Teljes hír megtekintése',
    noPushToken: 'Nem sikerült a token lekérdezése, az applikáció nem tud push értesítéseket fogadni!',
    noInternet: 'Nincs internetkapcsolat',
    news: 'Hírek',
    events: 'Események',
    grief: 'Gyászjelentők',
    loadMore: 'Következő 10 betöltése',
    accesToGallery: 'Szükségünk van a galériádhoz való hozzáféréshez hogy képet tölthess fel a hírdetés feladása során!',
    requiredField: 'Mező kitöltése kötelező!',
    maximum: 'Legtöbb',
    characterRestriction: 'karakter a megengedett!',
    noProfanity: 'Ne használj profán szavakat!',
    postingFaliure: 'Nem sikerült létrehozni a hírdetést!',
    sent: 'Elküldve',
    postPosts: 'Apróhirdetés feladása',
    postTitlePlaceholder: 'Hírdetés címe',
    postContactsPlaceholder: 'Elérhetőségek (tel, email...)',
    postContentPlaceholder: 'Hírdetés szövege',
    postImageUpload: 'Fénykép feltöltése',
    postSend: 'Hírdetés feladása',
    posts: 'Apróhirdetések',
    informations: 'Információk',

    church: 'Miserend',
    consil: 'Polgármesteri hivatal',
    consilContact: 'Ügyfélfogadás',
    consilContactAndRooms: 'Ügyfélfogadás és ügyfélosztályok',
    address: 'Cím',
    showOnMap: 'Mutasd a térképen',
    telephoneFax: 'Telefon / fax',
    mobileFax: 'Mobiltelefon',
    sportBase: 'Sportbázis',
    // Innen
    sportBaseTitle1: 'A sportbázis használatára igénylést Király Zsolt felelősnek kell letenni munkanapokon 12- 20 óra között.',
    sportBaseTitle2: 'A SPORTBÁZIS MŰKÖDÉSI SZABÁLYZATÁBÓL',
    sportBaseTitle3: 'BÉRLÉSI DÍJAK:',
    sportBaseTitle4: 'NEM KELL BÉRLETI DÍJAT FIZESSENEK AZ ALÁBBI HASZNÁLÓK:',
    sportBaseTitle5: 'A sportbázis órarendje',
    sportBaseText1: 'A sportbázis működéséért felel az alpolgármester. Közvetlen felelős Király Zsolt rekreációs ügyintéző. A sportbázis területén TILOS A DOHÁNYZÁS ÉS AZ ALKOHOLFOGYASZTÁS !',
    sportBaseText2: '- Tornaterem 10 lej / óra',
    sportBaseText3: '- Multifunkcionális műgyep pálya 20 lej / óra',
    sportBaseText4: '- A csomafalvi óvodások és tanulók',
    sportBaseText5: '- Az önkéntes tűzoltók hetente kétszer 2 órát',
    sportBaseText6: '- Az iskoláskorú fiatalok hetente négyszer 2 órát (ebből hétvégeken kétszer két órát)',
    sportBaseText7: '- Azon sportolók és csapatok, akik legalább zonális szinten képviselik községünket a sportversenyeken, hetente kétszer 2 órát.',
    sportBaseText8: 'Az előzőekben felsorolt személyeken kívül más használók bérleti díj kifizetése után vehetik igénybe a sportbázist. Alkalmi használat esetén 24 órával azelőtt kell szólni a felelős személynek és helyben ki lehet fizetni a használati díjat! Havi bérlet esetén az igénylést a hónap kezdete előtt le kell tenni. Ebben az esetben hónap végén számlát és nyugtát állítunk ki. Amennyiben nincs igény a sportbázis használatára, azt 24 órával azelőtt be kell jelenteni!',
    sportBaseText9: '- Naponta 7:30 és 22:00 között működik a sportbázis',
    sportBaseText10: '- Iskolaidőben 8:00 és 16:00 óra között, valamint hétvégén 8:00 és 22:00 óra között elsőbbséget élveznek a diákoknak szervezett rendezvények, utána a sportolók tevékenységei és végül a szabadidős programok.',
    sportBaseText11: 'Az aktuális hétre érvényes órarend hétfőnként 9 óráig ki lesz függesztve a tornateremnél és a sportpályánál, amit mindenki köteles betartson.',
    // Idáig
    mainContacts: 'Fontosabb elérhetőségek',
    contactPlace: 'Intézmény',
    contactAddress: 'Cím',
    contact1: 'Sürgősségi hívószám',
    contact2: 'Orvosi rendelő- Dr. Müzlinger Ildikó',
    contact3: 'Orvosi rendelő- Dr. Zrínyi Enikő Erika',
    contact4: 'Mezőgazdasági Kifizetési Ügynökség',
    contact5: 'Morara Miron Közjegyző',
    contact6: 'Országos Befektetési Társaság',
    contact7: 'Maros Vizügyi Igazgatóság',
    contact8: 'Közpénzügyi Általános Igazgatóság',
    contact9: 'Közegészségügyi Igazgatóság',
    contact10: 'Dél Erdélyi Villanyszolgálatató',
    contact11: 'Csíkszeredai Erdészeti Hivatal',
    contact12: 'Gyergyószentmiklósi Erdészeti Hivatal',
    contact13: 'Népességnyilvántartó Igazgatóság',
    contact14: 'Hargita Megyei Szociális és Gyermekvédelmi Igazgatóság',
    contact15: 'Városi Közellátás',
    contact16: 'Gyergyoszentmiklosi Bíróság',
    contact17: 'Magyar Konzulátus',
    contact18: 'Környezetvédelmi Felügyelőség',
    contact19: 'Hargita Megyei Kataszteri Hivatal',
    contact20: 'Hargita Megyei Tanács',
    contact21: 'Területi munkaügyi felügyelőség',
    contact22: 'Megyei nyugdíj- és társadalombiztosítási pénztár',
    contact23: 'Fogyasztóvédelmi hivatal',
    contact24: 'Megyei statisztikai igazgatóság',
    contact25: 'Építészeti felügyelőség',
    contact26: 'Tanfelügyelőség',
    contact27: 'Gyergyószentmiklósi Kincstár',
    contact28: 'Sólyom Csilla Mária Közjegyző',
    contact29: 'Pop Miorita Közjegyző',
    contact30: 'Gyergyószentmiklósi Magánerdészet',
    contact31: 'Hargita Megyei Prefektura',
    contact32: 'Gyergyócsomafalvi Polgármesteri Hivatal',
    contact33: 'Gyergyócsomafalvi Önkéntes Tűzoltók',
    contact34: 'Posta Hivatal',
    contact35: 'Csomafalvi Közbirtokosság',
    contact36: 'Csomafalvi Rendőrség',
    contact37: 'Köllő Miklós Általános Iskola',
    contact38: 'Közszolgálatásokat Felügyelő és Szabályzó Nemzeti Hatóság',
    downloadable: 'Letölthető űrlapok',
    website: 'Hivatalos weboldal'
}

export const ro = {
    settings: 'Setări',
    language: 'Limba',
    theme: 'Mod întunecat',
    madeby: 'Realizat de:',
    originalNews: 'Vizualizarea știrilor în întregime',
    noPushToken: 'Tokenul nu s-a putut fi recuperat, aplicația nu poate primi notificări push!',
    noInternet: 'Fără conexiune internet',
    news: 'Știri',
    events: 'Evenimente',
    grief: 'Necrolog',
    loadMore: 'Încărcarea următorilor 10',
    accesToGallery: 'Avem nevoie de accesul la galeria ta pentru a încărca o imagine când postăm anunțul!',
    requiredField: 'Câmp obligatoriu!',
    maximum: 'Cel mult',
    characterRestriction: 'caractere sunt permise!',
    noProfanity: 'Nu folosi cuvinte profane!',
    postingFaliure: 'Anunțul nu s-a realizat!',
    sent: 'Trimis',
    postPosts: 'Postarea unui anunț',
    postTitlePlaceholder: 'Titlul anunțului',
    postContactsPlaceholder: 'Contact (tel, e-mail ...)',
    postContentPlaceholder: 'Textul anunțului',
    postImageUpload: 'Încărcarea fotografiei',
    postSend: 'Trimitere!',
    posts: 'Anunțuri',
    informations: 'Informații',

    church: 'Biserica',
    consil: 'Primărie',
    consilContact: 'Recepția clienților',
    consilContactAndRooms: 'Recepția clienților',
    address: 'Adresa',
    showOnMap: 'Afișați pe hartă',
    telephoneFax: 'Telefon / fax',
    mobileFax: 'Telefon mobil',
    sportBase: 'Baza sportivă',
    // Innen
    sportBaseTitle1: 'Pentru utilizarea bazei sportive solicitarea se face la Király Zsolt în zilele lucrătoare între orele 12 și 20.',
    sportBaseTitle2: 'DIN REGULILE DE FUNCȚIONARE A BAZEI SPORTIVE',
    sportBaseTitle3: 'TAXE DE ÎNCHIRIERE:',
    sportBaseTitle4: 'NU PLĂTESC CHIRIE URMĂTORII UTILIZATORI',
    sportBaseTitle5: 'Orarul bazei sportive',
    sportBaseText1: 'Viceprimarul este responsabil pentru funcționarea bazei sportive. Responsabil direct: administratorul de recreere Király Zsolt; FUMATUL ȘI CONSUMUL DE ALCOOL ESTE INTERZIS în zona bazei sportive!',
    sportBaseText2: '- Sala de sport 10 lei / oră',
    sportBaseText3: '- Teren multifuncțional de gazon artificial 20 lei / oră',
    sportBaseText4: '- Copii din grădiniță și elevii din Ciumani',
    sportBaseText5: '- Pompierii voluntari două ori săptămânal timp de 2 ore',
    sportBaseText6: '- Tinerii de vârstă școlară patru ori săptămânal timp de 2 ore (din care două ori câte 2 ore în weekend)',
    sportBaseText7: '- Sportivii și echipele care reprezintă comuna noastră cel puțin la nivel zonal în competiții sportive, două ori pe săptămână câte 2 ore.',
    sportBaseText8: 'Utilizatorii, alții decât cei enumerați mai sus, pot folosi baza sportivă după plata chiriei. În caz de utilizare ocazională, persoana responsabilă trebuie anunțată cu 24 de ore înainte, iar taxa de utilizare poate fi achitată local! În cazul unui abonament lunar, cererea trebuie făcută înainte de începutul lunii. În acest caz, vom emite factură și chitanță la sfârșitul lunii. Dacă nu vreți să folosiți baza sportivă, anunțați cu 24 de ore înainte!',
    sportBaseText9: '- Baza sportivă este deschisă zilnic între orele 07:30 - 22:00',
    sportBaseText10: '- În timp de școală de la 8:00 la 16:00 și în weekend de la 8:00 la 22:00, au prioritate evenimentele pentru elevi, urmate de activitățile pentru sportivi și la urmă programele de agrement.',
    sportBaseText11: 'Programul săptămânii curente va fi afișat până la ora 9 dimineața în fiecare luni, la sala de sport și la terenul de sport, care trebuie respectată de toate lumea.',
    // Idáig
    mainContacts: 'Contacte importante',
    contactPlace: 'Numele Institutului',
    contactAddress: 'Adresa',
    contact1: 'Apel de urgenta',
    contact2: 'Cabinet medical- Dr. Müzlinger Ildkó',
    contact3: 'Cabinet medical- Dr. Zrínyi Enikő Erika',
    contact4: 'Agentia de Plati si Interventie pentru Agricultura',
    contact5: 'Biroul Notariat Morar Miron',
    contact6: 'Compania Nationala de Investitii',
    contact7: 'Directia Apelor Mures',
    contact8: 'Directia Generala a Finantelor Publice',
    contact9: 'Directia de Sanatate Publica',
    contact10: 'Electrica Furnizare Transilvania Sud',
    contact11: 'Ocolul Silvic de Regim Miercurea Ciuc',
    contact12: 'Ocolul Silvic de Regim Gheorgheni',
    contact13: 'Directia de Evidenta Populatiei',
    contact14: 'Direcţia Generală de Asistenţă Socială şi Protecţia Copilului Harghita',
    contact15: 'Gospodaria Oraseneasca',
    contact16: 'Judecatoria Gheorgheni',
    contact17: 'Consulatul General al Ungariei in Romania',
    contact18: 'Garda de Mediu Harghita',
    contact19: 'Oficiul de Cadastru, Geodezie şi Cartografie a Judeţului Harghita',
    contact20: 'Consiliul Judetean',
    contact21: 'Inspectoratul Teritorial de Muncă',
    contact22: 'Casa Judeţeană de Pensii şi Alte Drepturi de Asigurări Sociale',
    contact23: 'Oficiul Judeţean pentru Protecţia Consumatorilor',
    contact24: 'Direcţia Judeţeană de Statistică',
    contact25: 'Inspectoratul de Construcţii Judeţean',
    contact26: 'Inspectoratul Şcolar Judeţean',
    contact27: 'Administratia Finantelor Publice Gheorgheni Trezoreria Gheorgheni',
    contact28: 'Biroul Notariat Solyom Csilla Maria',
    contact29: 'Biroul Notariat Pop Miorita',
    contact30: 'Ocolul Silvic Particular Gheorgheni',
    contact31: 'Institutia Prefectului Judetului Harghita',
    contact32: 'Primaria Comunei Ciumani',
    contact33: 'Pompierii Voluntari Ciumani',
    contact34: 'Posta',
    contact35: 'Composesorat Ciumani',
    contact36: 'Politia Ciumani',
    contact37: 'Scoala Gimnaziala Köllő Miklós',
    contact38: 'Autoritatea Nationala De Reglementare Pentru Serv    - 1 -iciile Comunitare De Utilitati Publice (ANRSC)',
    downloadable: 'Formulare descărcabile',
    website: 'Site oficial'
}
