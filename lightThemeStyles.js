import { StyleSheet } from 'react-native';
import { lightTheme } from './constants'
import { Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const header = StyleSheet.create({
    container: {
      backgroundColor: lightTheme.accent,
      flexDirection: "row",
      alignItems: "center",
      position: "relative"
    },
   
    image: {
      top: 0,
      width: '100%',
      position: "absolute",
      bottom: 0,
      right: 0
    },
    
    csomafalvapp: {
      fontSize: 28,
      color: lightTheme.accentTextColor,
      backgroundColor: "transparent",
      marginLeft: 10,
    },
    rightIconButton: {
      padding: 5,
      alignItems: "center",
      marginRight:10
    },
    rightIcon: {
      backgroundColor: "transparent",
      color: lightTheme.accentTextColor,
      fontSize: 24
    },
    imageStack: {
      width: '100%',
      backgroundColor: lightTheme.accent,
      justifyContent:"space-between",
      position: "absolute",
      bottom: 0,
      flexDirection:'row'
    }
});


export const navbar = StyleSheet.create({
    container: {
        backgroundColor: lightTheme.background,
        flexDirection: "row",
        borderTopColor:lightTheme.accent,
        borderTopWidth:1,
    },
    buttonWrapper: {
        flex: 1,
        paddingTop: 8,
        paddingBottom: 10,
        paddingHorizontal: 5,
        width: 80,
        alignItems: "center",
    },
    activeButtonWrapper:{
        backgroundColor:lightTheme.accent,
        borderRadius:6,
        justifyContent: 'center',
    },
    icon: {
        backgroundColor: "transparent",
        color: lightTheme.textColor,
        fontSize: 20,
    },
    iconTitle: {
        fontSize: 12,
        color: lightTheme.textColor,
        backgroundColor: "transparent",
        paddingTop: 4,
        textAlign: 'center',
    },
    activeIcon: {
        backgroundColor: "transparent",
        color: lightTheme.background,
        fontSize: 20,
        opacity: 1
    },
    activeTitle: {
        fontSize: 12,
        fontWeight:'bold',
        textAlign: 'center',
        color: lightTheme.background,
        backgroundColor: "transparent",
        paddingTop: 4
    },
});

function elevationShadowStyle(elevation) {
    return {
      elevation,
      shadowColor: lightTheme.textColor,
      shadowOffset: { width: 3, height: 3 },
      shadowOpacity: 0.1,
      shadowRadius: 3
    };
}
  
export const card = StyleSheet.create({
    shadow: elevationShadowStyle(5),
    container: {
      borderWidth: 1,
      borderRadius: 5,
      borderColor: lightTheme.accent,
      flexWrap: "nowrap",
      backgroundColor: lightTheme.background,
      overflow: "hidden", 
    },
    cardItemImagePlace: {
      backgroundColor: lightTheme.accent,
      flex: 1,
      height: 210
    },
    buttonGroup: {
      padding: 8,
      flexDirection: "row",
      alignSelf: "flex-end",
      height: 75
    },
    icon3: {
      fontSize: 24,
      color: lightTheme.textColor
    },
    title: {
      top: 195,
      left: 6,
      position: "absolute",
      fontFamily: "roboto-700",
      color: lightTheme.textColor,
      fontSize: 16
    },
    text: {
      top: 225,
      left: 6,
      position: "absolute",
      fontFamily: "roboto-regular",
      color: lightTheme.textColor,
    }
});

export const newsCard = StyleSheet.create({
    container: {
      width: '90%',
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 0.5,
      shadowRadius: 20,
      borderWidth: 1,
      borderColor: lightTheme.accent,
      borderTopRightRadius: 50,
      borderBottomRightRadius: 50,
      overflow: "hidden",
      marginTop:20,
    },
    ellipse: {
      top: 0,
      left: 0,
      width: 175,
      height: 175,
      position: "absolute"
    },
    icon3: {
      top: 54,
      left: 106,
      position: "absolute",
      color: lightTheme.accentTextColor,
      fontSize: 60
    },
    ellipseStack: {
      width: 175,
      height: 175,
      position: 'relative'
    },
    loremIpsum: {
      fontFamily: "roboto-700",
      color: lightTheme.textColor,
      fontSize: 16,
      marginLeft: 8,
      width: '40%',
    },
    loremIpsum16: {
      marginLeft: 6,
      marginRight:20,
      fontFamily: "roboto-regular",
      color: lightTheme.textColor,
    },
    icon2: {
      top: 44,
      left: 307,
      position: "absolute",
      color: lightTheme.textColor,
      fontSize: 60
    },
    loremIpsum16Stack: {
      width: '60%',
      height: 104,
      marginTop: 15
    },
    loremIpsumColumn: {
      width: '100%',
      marginTop: 13,
      marginBottom: 14,
      overflow: 'hidden'
    },
    ellipseStackRow: {
      height: 150,
      flexDirection: "row",
      flex: 1,
      marginRight: 19,
      marginLeft: -98,
      marginTop: -13
    }
});

export const radioButton = StyleSheet.create({
    container: {
        width: '33.33%',
        height:'100%', 
        backgroundColor:lightTheme.background
      },
      buttonFace:{
        width: '100%',
        height:'100%',
        borderBottomWidth: 2, 
        alignItems: 'center', 
        justifyContent:'center'
      }
})

export const newsPage = StyleSheet.create({
    container: {
      flex: 1, alignItems:'center',backgroundColor: lightTheme.background,
    },
    moreButton:{
      marginTop:30, marginBottom:30,backgroundColor: lightTheme.accent,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
      borderRadius: 10,
      padding: 15,
      width: '85%',
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 1,
      shadowRadius: 10,
    },
    moreButtonText:{  
      color:lightTheme.accentTextColor, 
      fontWeight:'bold'
    },
    materialHeader: {
      height: '15%',
      width: '100%',
    }, 
    materialIconTextButtonsFooter: {
      width: '100%',
      bottom: 0,
      position: 'absolute',
    },
    image1: {
      width: '90%',
      height: '90%',
      opacity: 0.05,
      alignSelf: "center",
      position: "absolute",
      top: '20%'
    },
    buttonContainer:{ 
      height:45,
      flexDirection:'row', 
      justifyContent:'space-between'
    }
  });

  
export const posts = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: lightTheme.background
    },
    image: {
      width: "80%",
      height: "100%",
      position: "absolute",
      opacity: 0.05,
      left: '60%',
      top: '60%',
      transform: [{translateY: -windowHeight / 2 }, {translateX: -windowWidth / 2 }]
    },
    materialCardWithRightButtons2: {
      height: 266,
      width: '90%',
      elevation: 60,
      marginBottom:25,
      marginLeft:'5%'
    },
    moreButton:{
      marginTop:30, marginBottom:30,backgroundColor: lightTheme.accent,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
      borderRadius: 10,
      padding: 15,
      width: '90%',
      marginLeft:'5%',
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 1,
      shadowRadius: 10,
    },
    moreButtonText:{  
      color:lightTheme.accentTextColor, 
      fontWeight:'bold'
    },
    imageStack: {
      width: '100%',
      height: '75%',
      marginTop: 10,
      backgroundColor:'transparent',
    },
    materialHeader1: {
      height: '15%',
      width: '100%',
    },
    materialIconTextButtonsFooter: {
      width: '100%',
      bottom: 0,
      position: 'absolute',
    },
    addButtonWrapper:{
      position: 'absolute', 
      bottom: 80, 
      right: 10, 
      backgroundColor:lightTheme.accent,
      zIndex:3001, 
      padding:15, 
      borderRadius:50,
      borderColor:lightTheme.accentTextColor,
      borderWidth:1,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
    },
    addButtonIcon: {
      color: lightTheme.accentTextColor,
      fontSize: 30,
    }
  });

export  const settings = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: lightTheme.background
    },
    materialHeader1: {
      height: '15%',
      width: '100%',
    }, 
    materialIconTextButtonsFooter: {
      width: '100%',
      bottom: 0,
      position: 'absolute',
    },
    heading: {
      fontFamily: "roboto-700",
      color: lightTheme.accent,
      fontSize: 32,
      opacity: 0.25,
      marginTop:10,
      textAlign: "center"
    },
    madeby:{
      fontFamily: "roboto-700",
      color: lightTheme.accent,
      fontSize: 16,
      opacity: 0.25,
      marginTop:10,
      textAlign: "center"
    },
    rect: {
      width: '85%',
      height: 55,
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 0.6,
      shadowRadius: 10,
      flexDirection: "row",
      marginTop: 30,
    },
    pushErtesitesekRow: {
      height: 56,
      flexDirection: "row",
      flex: 1,
      marginLeft: 21
    },
    text: {
      fontFamily: "roboto-700",
      color:lightTheme.textColor,
      fontSize: 16
    },
    madeBy:{
      fontFamily: "roboto-700",
      color: lightTheme.textColor,
      fontSize: 16,
      marginTop: 30
    },
    icon: {
      color: lightTheme.textColor,
      fontSize: 20,
      height: 20,
      width: 20,
      marginLeft: 15
    },
    textRow: {
      flexDirection: "row",
      justifyContent:'space-between',
      alignItems: 'center',
      flex: 1,
      marginRight: 20,
      marginLeft: 20,
      marginTop: 10,
      marginBottom: 10
      
    },
  });

export const postPost = StyleSheet.create({
    container: {
      flex: 1, alignItems: 'center',backgroundColor: lightTheme.background
    },
    uploaded:{
      borderRadius: 5,
      borderWidth:2,
      borderColor: 'green',
      padding: 5,
      width: '85%',
      color: lightTheme.textColor,
      textAlign: 'center',
      fontWeight: 'bold'
    },
    buttonContainer:{
      backgroundColor: lightTheme.accent,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row",
      borderRadius: 5,
      height: 50,
      width: '85%',
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 1,
      shadowRadius: 10,
      marginTop: 50,
    },
    hirdetesFeladasa: {
      color: lightTheme.accentTextColor,
      fontSize: 14,
      width: '70%'
    },
    sendIcon: {
      color: lightTheme.textColor,
      fontSize: 20,
      right: 16,
    },
    materialHeader1: {
      height: '15%',
      width: '100%',
    }, 
    materialIconTextButtonsFooter: {
      width: '100%',
      bottom: 0,
      position: 'absolute',
    },
    hasznosInformaciok1: {
      fontFamily: "roboto-700",
      color: lightTheme.accent,
      fontSize: 36,
      opacity: 0.25,
      marginTop: 14,
    },
    rect1: {
      borderRadius: 5,
      width: '85%',
      height: 100,
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 0.6,
      shadowRadius: 10,
      marginTop: 30,
    },
    hirdetesCime: {
      fontFamily: "roboto-regular",
      color: lightTheme.textColor,
      fontSize: 16,
      marginTop: 5,
      marginLeft: 15
    },
    rect2: {
      borderRadius: 5,
      width: '85%',
      minHeight: 220,
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 0.6,
      shadowRadius: 10,
      overflow: "scroll",
      marginTop: 20,
    },
    hirdetesSzovege: {
      fontFamily: "roboto-regular",
      color: lightTheme.textColor,
      fontSize: 16,
      marginTop: 5,
      marginLeft: 15
    },
    rect3: {
      borderRadius: 5,
      width: '85%',
      height: 63,
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 0.6,
      shadowRadius: 10,
      flexDirection: "row",
      marginTop: 20,
    },
    loremIpsum1: {
      fontFamily: "roboto-700",
      color:  lightTheme.textColor,
      fontSize: 12,
      marginTop: 7
    },
    icon: {
      color:  lightTheme.textColor,
      fontSize: 30,
      height: 33,
      width: 30,
      marginLeft: 27
    },
    loremIpsum1Row: {
      minHeight: 33,
      flexDirection: "row",
      flexWrap:"wrap",
      flex: 1,
      marginRight: 12,
      marginLeft: 15,
      marginTop: 16
    },
    materialButtonViolet: {
      height: 50,
      width: '80%',
      shadowColor:  lightTheme.textColor,
      shadowOffset: {
        width: 3,
        height: 3
      },
      elevation: 15,
      shadowOpacity: 1,
      shadowRadius: 10,
      marginTop: 90,
    }
  });

export const newPage = StyleSheet.create({
    container: {
      flex: 1, backgroundColor: lightTheme.background
    },
    church:{
      width:'100%',
      height:'100%',
    },
    materialHeader: {
      height: '15%',
      width: '100%',
    }, 
    materialIconTextButtonsFooter: {
      width: '100%',
      bottom: 0,
      position: 'absolute',
    },
    image2: {
      width: '100%',
      height: '25%',
      backgroundColor: lightTheme.accent
    },
    rect: {
      top: '35%',
      right: 0,
      width: '85%',
      height: "50%",
      position: "absolute",
      backgroundColor: lightTheme.background,
      shadowColor: lightTheme.textColor,
      shadowOffset: {
        height: -3,
        width: -3
      },
      elevation: 30,
      shadowOpacity: 0.6,
      shadowRadius: 10,
      overflow: "scroll",
    },
    eladoVagyTudomisen: {
      marginTop: 20,
      marginBottom:20,
      fontFamily: "roboto-700",
      color: "#121212",
      fontSize: 24,
      left: 0
    },
    loremIpsum1: {
      top: 0,
      position: "absolute",
      fontFamily: "roboto-700",
      color: lightTheme.textColor,
      fontSize: 16,
      right: 0,
      opacity: 0.5
    },
    titeContainer: {
      width: '85%',
      marginLeft: 12
    },
    image1: {
      width: 200,
      height: 200,
      position: "absolute",
      opacity: 0.05,
      top: 0,
      left: 54
    },
    loremIpsum: {
      fontFamily: "roboto-regular",
      color: lightTheme.textColor,
      textAlign: "justify",
      left: 0,
      width: '85%',
      fontSize: 16,
    },
    image1Stack: {
      width: "100%",
      marginLeft: 12,
      paddingBottom:15,
      minHeight:325
    },
    image2Stack: {
      width: '100%',
    }
  });

export const infoPage =  StyleSheet.create({
  container: {
    width: '95%', 
    borderRadius:5,
    alignItems: 'flex-start', 
    borderColor: 'transparent', 
    backgroundColor: lightTheme.accent,
    shadowColor: lightTheme.textColor,
      shadowOffset: {
        height: -3,
        width: -3
      },
    elevation: 10,
    shadowOpacity: 0.6,
    shadowRadius: 10,
  },
  title: {
    color: lightTheme.accentTextColor,
    fontSize: 20
  },
  tableText:{
    textAlign: 'center',
    color: lightTheme.accentTextColor
  },
  subTitle: {
    color: lightTheme.accentTextColor, 
    fontSize: 20, 
    marginTop: 20,
    marginBottom:5
  },
  text:{
    color: lightTheme.accentTextColor, 
    fontSize: 16, 
    marginBottom:10
  },
  listItem:{
    color: lightTheme.accentTextColor, 
    fontSize: 16, 
    width:'100%'
  }
})