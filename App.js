import React, { useState, useEffect } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import AppLoading from "expo-app-loading";

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import * as Font from "expo-font";
import Hirdetesek from "./src/screens/Hirdetesek";
import Hirek from "./src/screens/Hirek";
import Hirdetesfeladasa from "./src/screens/Hirdetesfeladasa";
import Hiroldala from "./src/screens/Hiroldala";
import SettingsPage from "./src/screens/SettingsPage";
import InformationScreen from "./src/screens/InformationScreen";
import reducer from './src/reducers';

const DrawerNavigation = createDrawerNavigator({
 
  Hirek: Hirek,
  Hirdetesek: Hirdetesek,
  Hirdetesfeladasa: Hirdetesfeladasa,
  Hiroldala: Hiroldala,
  SettingsPage: SettingsPage,
  InformationScreen: InformationScreen
});

const StackNavigation = createStackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
   
    Hirek: Hirek,
    Hirdetesek: Hirdetesek,
    Hirdetesfeladasa: Hirdetesfeladasa,
    Hiroldala: Hiroldala,
    SettingsPage: SettingsPage,
    InformationScreen: InformationScreen
  },
  {
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(StackNavigation);

function App() {
  const store = createStore(reducer);
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return isLoadingComplete ?  <Provider store={store}><AppContainer /></Provider> : <AppLoading />;
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "roboto-700": require("./src/assets/fonts/roboto-700.ttf"),
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf")
    })
  ]);
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;
