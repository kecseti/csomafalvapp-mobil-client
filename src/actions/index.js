export const switchTheme = () => (
  { type: 'SWITCH_STYLE', }
);

export const switchLanguage = (language) => (
  { type: 'SWITCH_LANGUAGE', payload: language}
);