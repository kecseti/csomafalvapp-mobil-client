import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';

class Button extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      opacity: 1
    }
  }

  switchState = () =>{
    this.setState({opacity: this.state.opacity ===  1 ? 0.5 :  1})
    this.props.changeState();
  }

  render(){
    return (
      <View style={this.props.style.radioButton.container}>
        <TouchableOpacity onPress = {this.switchState}>
            <View style={{...this.props.style.radioButton.buttonFace, ...{borderBottomColor:this.props.style.header.imageStack.backgroundColor, }, ...{opacity: this.state.opacity}}}>
              <Icon name={this.props.iconName} type={this.props.iconFamily} size={16} color={this.props.style.header.imageStack.backgroundColor} style={{...{opacity: this.state.opacity}}}/>
              <Text style={{...{fontSize:12},...{color: this.props.style.header.imageStack.backgroundColor},...{opacity: this.state.opacity}}}> {this.props.text} </Text>
            </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(Button);