import React from "react";
import { View, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';

function CupertinoSegmentWithIcons(props) {
  return (
    <View style={[props.reducerStyle.newsCard.container, props.style]}>
      <View style={props.reducerStyle.newsCard.ellipseStackRow}>
        <View style={props.reducerStyle.newsCard.ellipseStack}>
          <Svg viewBox="0 0 175 175" style={props.reducerStyle.newsCard.ellipse}>
            <Ellipse
              stroke="rgba(230, 230, 230,1)"
              strokeWidth={0}
              fill={props.reducerStyle.header.imageStack.backgroundColor}
              cx={88}
              cy={88}
              rx={88}
              ry={88}
            ></Ellipse>
          </Svg>
          <Icon name={props.iconName} type={props.iconFamily} size={40} color={props.reducerStyle.newsCard.icon3.color} style={{positon:'absolute', top: 45, left: 45, width: 175, height: 175}} ></Icon>
        </View>
        <View style={props.reducerStyle.newsCard.loremIpsumColumn}>
          <Text style={props.reducerStyle.newsCard.loremIpsum}>{props.title}</Text>
          <View style={props.reducerStyle.newsCard.loremIpsum16Stack}>
            <Text style={props.reducerStyle.newsCard.loremIpsum16} numberOfLines={3}>{props.description}</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const mapStateToProps = (state) => {
  const reducerStyle = state.style
  return { reducerStyle }
};

export default connect(mapStateToProps)(CupertinoSegmentWithIcons);
