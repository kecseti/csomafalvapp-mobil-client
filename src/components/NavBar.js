import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';

function NavBar(props) {
  const firstActiveWrapper = props.active === 1 ? props.reducerStyle.navbar.activeButtonWrapper : {};
  const firstActiveIcon = props.active === 1 ? props.reducerStyle.navbar.activeIcon: {};
  const firstActiveText = props.active === 1 ? props.reducerStyle.navbar.activeTitle : {};
  const secondActiveWrapper = props.active === 2 ? props.reducerStyle.navbar.activeButtonWrapper : {};
  const secondActiveIcon = props.active === 2 ? props.reducerStyle.navbar.activeIcon: {};
  const secondActiveText = props.active === 2 ? props.reducerStyle.navbar.activeTitle : {};
  const thirdActiveWrapper = props.active === 3 ? props.reducerStyle.navbar.activeButtonWrapper : {};
  const thirdActiveIcon = props.active === 3 ? props.reducerStyle.navbar.activeIcon: {};
  const thirdActiveText = props.active === 3 ? props.reducerStyle.navbar.activeTitle : {};
  return (
    <View style={[props.reducerStyle.navbar.container, props.style]}>
      <TouchableOpacity style={props.reducerStyle.navbar.buttonWrapper} onPress={() => props.navigation.push('Hirdetesek')}>
        <View style={{...{width:'100%', height:'100%',alignItems: "center",}, ...firstActiveWrapper}}>
          <MaterialCommunityIconsIcon name="newspaper" style={{...props.reducerStyle.navbar.icon,...firstActiveIcon}}></MaterialCommunityIconsIcon>
          <Text style={{...props.reducerStyle.navbar.iconTitle,...firstActiveText}}>{props.reducerStyle.language.posts}</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={{...props.reducerStyle.navbar.buttonWrapper}} onPress={() => props.navigation.push('Hirek')}>
        <View style={{...{width:'100%', height:'100%',alignItems: "center",}, ...secondActiveWrapper}}>
          <Icon name="newspaper-o" style={{...props.reducerStyle.navbar.icon,...secondActiveIcon}}></Icon>
          <Text style={{...props.reducerStyle.navbar.iconTitle,...secondActiveText}}>{props.reducerStyle.language.news}</Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={props.reducerStyle.navbar.buttonWrapper} onPress={() => props.navigation.navigate('InformationScreen')}>
        <View style={{...{width:'100%', height:'100%',alignItems: "center"},...thirdActiveWrapper}}>
          <MaterialCommunityIconsIcon name="information-outline" style={{...props.reducerStyle.navbar.icon,...thirdActiveIcon}} ></MaterialCommunityIconsIcon>
          <Text style={{...props.reducerStyle.navbar.iconTitle,...thirdActiveText}}>{props.reducerStyle.language.informations}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const mapStateToProps = (state) => {
  const reducerStyle = state.style
  return { reducerStyle }
};

export default connect(mapStateToProps)(NavBar);