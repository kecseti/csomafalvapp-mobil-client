import React from "react";
import { View, ImageBackground, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from 'react-redux';

function Header(props) {
  return (
    <View style={[props.reducerStyle.header.container, props.style]}>
      <ImageBackground source={require("../assets/images/panel.jpg")} resizeMode="cover"  style={props.reducerStyle.header.image} imageStyle={props.reducerStyle.header.image_imageStyle}></ImageBackground>
      <View style={props.reducerStyle.header.imageStack}>
          <Text numberOfLines={1} style={props.reducerStyle.header.csomafalvapp}> Csomafalvapp </Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('SettingsPage')} style={props.reducerStyle.header.rightIconButton}>
            <Icon name="md-settings" style={props.reducerStyle.header.rightIcon}></Icon>
          </TouchableOpacity>
      </View>
    </View>
  );
}


const mapStateToProps = (state) => {
  const reducerStyle = state.style
  return { reducerStyle }
};

export default connect(mapStateToProps)(Header);
