import React from "react";
import {  View, Image, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';

function MaterialCardWithRightButtons2(props) {
  return (
      <View style={[props.reducerStyle.card.container, props.style, props.reducerStyle.card.shadow]}>
        <Image source={{uri:props.imageURL}} resizeMode="cover" style={props.reducerStyle.card.cardItemImagePlace} ></Image>
        <View style={props.reducerStyle.card.buttonGroup}>
            <Icon name="eye" style={props.reducerStyle.card.icon3}></Icon>
        </View>
        <Text style={props.reducerStyle.card.title}>{props.title}</Text>
        <Text numberOfLines={2} ellipsizeMode='tail' style={props.reducerStyle.card.text}>{props.text}</Text>
      </View>
  );
}

const mapStateToProps = (state) => {
  const reducerStyle = state.style
  return { reducerStyle }
};

export default connect(mapStateToProps)(MaterialCardWithRightButtons2);
