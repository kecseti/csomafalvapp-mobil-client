import { combineReducers } from 'redux';
import { header,card,navbar,newPage,newsCard,newsPage,postPost,posts,radioButton,settings, infoPage } from '../../lightThemeStyles';
import { headerDark,cardDark,navbarDark,newPageDark,newsCardDark,newsPageDark,postPostDark,postsDark,radioButtonDark,settingsDark,infoPageDark } from '../../darkThemeStyles'
import { hu, ro } from '../../language'

import AsyncStorage from '@react-native-community/async-storage';

let INITIAL_STATE = { header, card, navbar, newPage, newsCard, newsPage, postPost, posts, radioButton, settings, infoPage, language: hu };

async function init() {
  const initialTheme = await AsyncStorage.getItem('theme');
  const initialLanguage = await AsyncStorage.getItem('language');

  if(initialLanguage == 'hu'){
    language = hu;
  } else {
    language = ro;
  }

  if(initialTheme === 'dark'){ 
    INITIAL_STATE = { header: headerDark, card: cardDark, navbar: navbarDark, newPage: newPageDark, newsCard: newsCardDark, newsPage:newsPageDark, postPost: postPostDark, posts: postsDark, radioButton: radioButtonDark, settings: settingsDark,infoPage: infoPageDark, language}
  } else {
    INITIAL_STATE = { header, card, navbar, newPage, newsCard, newsPage, postPost, posts, radioButton, settings, infoPage, language };
  }
} 

const initialTheme =  init();

const stylesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SWITCH_STYLE':
      if(state.header.container.backgroundColor === header.container.backgroundColor){
        return {header: headerDark, card: cardDark, navbar: navbarDark, newPage: newPageDark, newsCard: newsCardDark, newsPage:newsPageDark, postPost: postPostDark, posts: postsDark, radioButton: radioButtonDark, settings: settingsDark, infoPage: infoPageDark, language: state.language}
      } else {
        return {header, card, navbar, newPage, newsCard, newsPage, postPost, posts, radioButton, settings, infoPage ,language: state.language}
      }
    case 'SWITCH_LANGUAGE':
      if(action.payload == 'hu'){
        language = hu;
        return { header:state.header, card: state.card, navbar: state.navbar, newPage: state.newPage, newsCard: state.newsCard, newsPage: state.newsPage, postPost: state.postPost, posts: state.posts, radioButton: state.radioButton, settings: state.settings, infoPage: state.infoPage, language}
      } else {
        language = ro;
        return { header:state.header, card: state.card, navbar: state.navbar, newPage: state.newPage, newsCard: state.newsCard, newsPage: state.newsPage, postPost: state.postPost, posts: state.posts, radioButton: state.radioButton, settings: state.settings, infoPage: state.infoPage, language}
      }
    default:
      return state;
  } 
};

export default combineReducers({ 
    style: stylesReducer
});