import React from "react";
import { View, ActivityIndicator, Image, ScrollView, Text,TouchableOpacity } from "react-native";
import NavBar from "../components/NavBar";
import Header from "../components/Header";
import Button from "../components/Button";
import Card  from "../components/NewsCard";
import { connect } from 'react-redux';

import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';

import * as Network from 'expo-network';

class Hirek extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      news: true, 
      events: true,
      church: false,
      grief: true,
      options: 11,
      upperLimit: 10,
      count: 0,
      errorMessage: undefined
    }
  }

  setNews = async () => { 
    let number = 0;
    if(!this.state.news === true) number = Number(number) + Number(1);
    if(this.state.events === true) number = Number(number) + Number(2);
    if(this.state.church === true) number = Number(number) + Number(4);
    if(this.state.grief === true) number = Number(number) + Number(8);
    if(number === 0) number = Number(11)
    const {result, count} = await this.fetchingData(number, 10);
    this.setState({cards: result, count, news: !this.state.news, upperLimit: 10, options: Number(number)});
  }
  setEvents = async () => { 
    let number = 0;
    if(this.state.news === true) number = Number(number) + Number(1);
    if(!this.state.events === true) number = Number(number) + Number(2);
    if(this.state.church === true) number = Number(number) + Number(4);
    if(this.state.grief === true) number = Number(number) + Number(8);
    if(number === 0) number = Number(11)
    const {result, count} = await this.fetchingData(number, 10);
    this.setState({cards: result,count, events: !this.state.events, upperLimit: 10, options: Number(number)});
  }
  setChurch = async () => { 
    let number = 0;
    if(this.state.news === true) number = Number(number) + Number(1);
    if(this.state.events === true) number = Number(number) + Number(2);
    if(!this.state.church === true) number = Number(number) + Number(4);
    if(this.state.grief === true) number = Number(number) + Number(8);
    if(number === 0) number = Number(11)
    const {result, count} = await this.fetchingData(number, 10);
    this.setState({cards: result,count, church: !this.state.church, upperLimit: 10, options: Number(number)});
  }
  setGrief = async () => { 
    let number = 0;
    if(this.state.news === true) number = Number(number) + Number(1);
    if(this.state.events === true) number = Number(number) + Number(2);
    if(this.state.church === true) number = Number(number) + Number(4);
    if(!this.state.grief === true) number = Number(number) + Number(8);
    if(number === 0) number = Number(11)
    const {result, count} = await this.fetchingData(number, 10);
    this.setState({cards: result,count, grief: !this.state.grief, upperLimit: 10, options: Number(number)});
  }

  registerForPushNotificationsAsync = async () => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert(this.props.style.language.noPushToken);
        return;
      }
      const token = (await Notifications.getExpoPushTokenAsync()).data;

      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      const raw = JSON.stringify({"token":token});
      const requestOptions = {method: 'POST',headers: myHeaders,body: raw,};
      fetch("https://csomafalapi.vercel.app/api/tokens", requestOptions)
        .then(response => response.json())
        .then( response => console.log('Okay'))
        .catch(error => { alert(this.props.style.language.noPushToken); });
  
    } 
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 125, 250],
        lightColor: '#FF231F7C',
      });
    }
  };
  

  async componentDidMount(){
    const network = await Network.getNetworkStateAsync();
    if(!network.isInternetReachable){
      this.setState({errorMessage:this.props.style.language.noInternet})
    }
    await this.registerForPushNotificationsAsync();
    const {result, count} = await this.fetchingData(this.state.options, this.state.upperLimit);
    this.setState({cards: result,count});
  }

  fetchingData = async (options, upperLimit) =>{
    return fetch(`https://csomafalapi.vercel.app/api/news/${Number(upperLimit) - 10}/${Number(upperLimit)}/${options}`,{method: 'GET',})
    .then(response => response.json())
    .then(result => {return {result: result.news, count: result.count}})
  }

  loadMore = async () => {
    const {result, count} = await this.fetchingData(this.state.options, this.state.upperLimit + 10);
    const fetchedCards = result;
    const oldCards = this.state.cards;
    const newCards = oldCards.concat(fetchedCards);

    this.setState({cards: newCards,count, upperLimit: this.state.upperLimit + 10});
  }

  render() {
    return (
      <View style={this.props.style.newsPage.container}>
        <Header style={this.props.style.newsPage.materialHeader} navigation={this.props.navigation}></Header>
        <View style={this.props.style.newsPage.buttonContainer}>
          <Button text = {this.props.style.language.news} iconName = 'newspaper-o' iconFamily= 'font-awesome' changeState={this.setNews}/>
          <Button text = {this.props.style.language.events} iconName = 'event' iconFamily= 'MaterialIcons' changeState={this.setEvents}/>
          <Button text = {this.props.style.language.grief} iconName = 'cross' iconFamily= 'font-awesome-5' changeState={this.setGrief}/>
        </View>
        <Image source={require("../assets/images/home.png")} resizeMode="contain" style={this.props.style.newsPage.image1} ></Image>
        <View style={{width: '100%', height: '70%', marginBottom: 70}}>
          <ScrollView contentContainerStyle={{alignItems: 'center', paddingBottom: 30}}>
            {
            this.state.cards.length === 0 
              ? 
              <ActivityIndicator style={{paddingTop: 100}} size={50} color={this.props.style.posts.moreButton.backgroundColor} /> :
              this.state.errorMessage 
              ?
              <Text style={{width:'100%', padding: 100, fontWeight:"bold", color:this.props.style.posts.moreButton.backgroundColor}}>{this.state.errorMessage}</Text> : 
            
            this.state.cards.map((data, id)=> {
                let iconName = 'newspaper-o';   // 1
                let iconFamily ='font-awesome';

                if(data.type===2){
                  iconName = 'event';   // 2
                  iconFamily ='MaterialIcons';
                }
                
                if(data.type===3){
                  iconName = 'church';   // 3
                  iconFamily ='font-awesome-5';
                }

                if(data.type===4){
                  iconName = 'cross';   // 4
                  iconFamily ='font-awesome-5';
                }

                return (
                  <TouchableOpacity key={data._id} onPress = { () => { this.props.navigation.navigate('Hiroldala', {id: data._id,url: data.url, imageURL: '', churchURL: data.photoURL, title:data.title, description: data.description, date: data.toSort})}}>
                    <Card iconName = {iconName} iconFamily= {iconFamily} title = {data.title} description = {data.description} />
                  </TouchableOpacity>
                  )
                }
              )
            }
            {
              this.state.count > this.state.upperLimit ? 
              <TouchableOpacity style={this.props.style.newsPage.moreButton} onPress={this.loadMore}>
                <Text style={this.props.style.newsPage.moreButtonText}>{this.props.style.language.loadMore}</Text>
              </TouchableOpacity>
              : <></>
            }
          </ScrollView>
        </View>
        
        <NavBar style={this.props.style.newsPage.materialIconTextButtonsFooter} active={2} navigation={this.props.navigation}></NavBar>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(Hirek);
