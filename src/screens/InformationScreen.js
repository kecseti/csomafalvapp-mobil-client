import React, { useState, useEffect} from "react";
import { View, SafeAreaView, Text, Image, ScrollView, TouchableOpacity, Linking, TableWrapper, Button } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import NavBar from "../components/NavBar";
import Header from "../components/Header";
import { connect } from 'react-redux';
import CollapsibleView from "@eliav2/react-native-collapsible-view";
import { Table, Row, Rows } from 'react-native-table-component';

function InformationScreen(props) {
    // TODO: Ajánlat: alfalu, szárhegy, DITRÓ, remete, újfalu, gyergyó, 
    // TODO: l10n
    // TODO: miserend - load actual miserend -> https://csomafalapi.vercel.app/api/news/0/3/4
    const tableHead = [props.style.language.contactPlace, 'Telefon', props.style.language.address];
    const tableData = [
        [props.style.language.contact1, '112', '-'],
        [props.style.language.contact2, '0266 351305', 'Nr. 221, Ciumani'],
        [props.style.language.contact3, '0266 351306', 'Nr. 221, Ciumani'],
        [props.style.language.contact4, '0266 361 211', 'Str.Stadionului nr.3, Gheorgheni'],
        [props.style.language.contact5, '0266 364908', 'Str. Kossuth Lajos nr.7, Gheorgheni'],
        [props.style.language.contact6, '021 3167381', 'Str. Horatiu nr.18, Bucuresti 1, 010834'],
        [props.style.language.contact7, '0265 260289', 'Str. Aleea Carpati nr. 61, Tirgu Mures, jud.Mures'],
        [props.style.language.contact8, '0266-207817', 'Str. Revolutie din Decembrie nr.20, Miercurea Ciuc'],
        [props.style.language.contact9, '0266 324483', 'Str. Miko nr.1, Miercurea Ciuc'],
        [props.style.language.contact10, '0266 365349', '-'],
        [props.style.language.contact11, '0266 313222', 'Str. Leliceni nr.6, Gheorgheni'],
        [props.style.language.contact12, '0266 364363', 'Str. Gabor Aron nr.2, Gheorgheni'],
        [props.style.language.contact13, '0266 364817', '-'],
        [props.style.language.contact14, '0266 310192', 'Piata Libertatii nr.5, Miercurea Ciuc'],
        [props.style.language.contact15, '0266 364365', 'Str. Gabor Aron nr.34, Gheorgheni'],
        [props.style.language.contact16, '0266 364902', 'Str. Kossuth Lajos nr.2, Gheorgheni'],
        [props.style.language.contact17, '0266 207335', 'Str. Petőfi Sándor nr.45, Miercurea Ciuc'],
        [props.style.language.contact18, '0266 312766', 'Str. George Cosbuc nr.43, Miercurea Ciuc'],
        [props.style.language.contact19, '0266 313611', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact20, '0266 207700', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact21, '0266 371662', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact22, '0266 371204', 'Str. Kossuth Lajos nr.94, Miercurea Ciuc'],
        [props.style.language.contact23, '0266 312532', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact24, '0266 371407', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact25, '0266 371429', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact26, '0266 312514', 'Piata Libertatii nr.5., Miercurea Ciuc'],
        [props.style.language.contact27, '0266 364837', '-'],
        [props.style.language.contact28, '0266 365054', 'Str. Kossuth Lajos nr.6, Gheorgheni'],
        [props.style.language.contact29, '0266 365075', 'Piata Libertatii nr.24, Gheorgheni'],
        [props.style.language.contact30, '0266 361786', 'Piata Libertatii nr.23, Gheorgheni'],
        [props.style.language.contact31, '0266 371114', 'Piata Libertatii nr.5, Miercurea Ciuc'],
        [props.style.language.contact32, '0266 351006', 'Piata Borsos Miklós, nr. 208, Ciumani'],
        [props.style.language.contact33, '0752886332\n0733 068188', 'Piata Borsos Miklós, nr. 208, Ciumani'],
        [props.style.language.contact34, '0266 351309', 'Nr.208, Ciumani'],
        [props.style.language.contact35, '0266 351221', 'Nr. 202, Ciumani'],
        [props.style.language.contact36, '0266 351001', 'Nr. 208, Ciumani'],
        [props.style.language.contact37, '0266 351005', 'Nr. 209/A, Ciumani'],
        [props.style.language.contact38, '0266 310 132', 'Piata Libertatii nr. 5, Mun. Miercurea-Ciuc, Jud. Harghita'],
      ]
      
    const [churcURI, setChurchUrl] = useState("");

    useEffect(() => {
      async function fetchMyAPI() { 
        const fetched = await fetch(`https://csomafalapi.vercel.app/api/news/0/1/4`,{method: 'GET',});
        const jsonFetched = await fetched.json()
        const url = jsonFetched.news[0];  
        setChurchUrl(url); 
      }
      fetchMyAPI()
    }, [])

  return (
    <SafeAreaView style={props.style.settings.container}>
        <Header style={props.style.settings.materialHeader1} navigation={props.navigation}></Header>
        <View style={{width: '100%', height: '100%', paddingBottom: 200}}>
          <ScrollView contentContainerStyle={{alignItems: 'center'}}>
            <Text style={{...props.style.settings.heading,...{marginTop:20}}}>{props.style.language.informations}</Text>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.church}</Text>}>
                <Image source={{uri: churcURI.photoURL}} resizeMode="contain" style={{height:500}} ></Image>
            </CollapsibleView>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.consil}</Text>}>

                <Text style={props.style.infoPage.subTitle}>{props.style.language.consilContact}</Text>
                <TouchableOpacity style ={{ width: '100%', }} onPress={() => Linking.openURL('http://www.csomafalva.info/sites/default/files/Orsi/Bogi/orarend.pdf') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>{props.style.language.consilContactAndRooms}</Text>
                            <FontAwesomeIcon name="external-link"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>

                <Text style={props.style.infoPage.subTitle}>{props.style.language.address}</Text>
                <TouchableOpacity style ={{ width: '100%', marginTop:0 }} onPress={() => Linking.openURL('geo:46.6799973,25.519454') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>{props.style.language.showOnMap}</Text>
                            <MaterialIcons name="location-on"  style={props.style.settings.icon} ></MaterialIcons>
                        </View>
                    </View>
                </TouchableOpacity>

                <Text style={props.style.infoPage.subTitle}>{props.style.language.telephoneFax}</Text>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('tel:+40266351006') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>+40-266-351006</Text>
                            <FontAwesomeIcon name="phone"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>

                <Text style={props.style.infoPage.subTitle}>{props.style.language.mobileFax}</Text>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('tel:+40733068184') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>+40-733-068184</Text>
                            <FontAwesomeIcon name="phone"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>

                <Text style={props.style.infoPage.subTitle}>Email</Text>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('mailto:csomafalva@csomafalva.info')  }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>csomafalva@csomafalva.info</Text>
                            <MaterialIcons name="email"  style={props.style.settings.icon} ></MaterialIcons>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('mailto:consiliul_ciumani@yahoo.com') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:10}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>consiliul_ciumani@yahoo.com</Text>
                            <MaterialIcons name="email"  style={props.style.settings.icon} ></MaterialIcons>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('mailto:ciumani@hr.e-adm.ro') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:10}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>ciumani@hr.e-adm.ro</Text>
                            <MaterialIcons name="email"  style={props.style.settings.icon} ></MaterialIcons>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('mailto:csomafalvi_polgmhiv@yahoo.com') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:10}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>csomafalvi_polgmhiv@yahoo.com</Text>
                            <MaterialIcons name="email"  style={props.style.settings.icon} ></MaterialIcons>
                        </View>
                    </View>
                </TouchableOpacity>

            </CollapsibleView>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.sportBase}</Text>}>
                <Text style={props.style.infoPage.subTitle}>{props.style.language.sportBaseTitle1}</Text>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('tel:+40753587553') }>
                    <View style={{...props.style.settings.rect,...{width: '100%', marginTop:0, marginBottom:15}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>+40753 587 553</Text>
                            <FontAwesomeIcon name="phone"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>
                <Text style={props.style.infoPage.subTitle} >{props.style.language.sportBaseTitle2}</Text>
                <Text style={props.style.infoPage.text}>{props.style.language.sportBaseText1} </Text>
                <Text style={props.style.infoPage.subTitle} >{props.style.language.sportBaseTitle3}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText2}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText3}</Text>
                <Text style={props.style.infoPage.subTitle}>{props.style.language.sportBaseTitle4} </Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText4}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText5}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText6}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText7}</Text>  
                <Text style={{...props.style.infoPage.text,...{marginTop:15}}}>{props.style.language.sportBaseText8}</Text>
                <Text style={props.style.infoPage.subTitle}>{props.style.language.sportBaseTitle5}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText9}</Text>
                <Text style={props.style.infoPage.listItem}>{props.style.language.sportBaseText10}</Text>
                <Text style={{...props.style.infoPage.text,...{marginTop:15}}}>{props.style.language.sportBaseText11}</Text>
            </CollapsibleView>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.mainContacts}</Text>}>
                <Table borderStyle={{borderColor: 'transparent' }}>
                    <Row data={tableHead} style={{ height: 40, backgroundColor: props.style.infoPage.tableText.color }} textStyle={{ textAlign: 'center', color: props.style.infoPage.container.backgroundColor  }}/>
                    {
                        tableData.map((rowData, index) => (
                            <Row key={index} data={rowData} style={[{  padding: 7 }, index%2 && {borderTopWidth:1, borderBottomWidth:1, borderColor: props.style.infoPage.title.color}]} textStyle={props.style.infoPage.tableText}
                            />
                        ))
                    }
                </Table>
            </CollapsibleView>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.downloadable}</Text>}>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('http://www.csomafalva.info/letoltheto-urlapok') }>
                    <View style={{...props.style.settings.rect,...{width: '100%'}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>{props.style.language.downloadable}</Text>
                            <FontAwesomeIcon name="external-link"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>
            </CollapsibleView>

            <CollapsibleView style={props.style.infoPage.container} arrowStyling={{color:props.style.infoPage.title.color }} title={<Text style={props.style.infoPage.title}>{props.style.language.website}</Text>}>
                <TouchableOpacity style ={{ width: '100%' }} onPress={() => Linking.openURL('http://www.csomafalva.info') }>
                    <View style={{...props.style.settings.rect,...{width: '100%'}}}>
                        <View style={props.style.settings.textRow}>
                            <Text style={props.style.settings.text}>{props.style.language.website}</Text>
                            <FontAwesomeIcon name="external-link"  style={props.style.settings.icon} ></FontAwesomeIcon>
                        </View>
                    </View>
                </TouchableOpacity>
            </CollapsibleView>

            <Text style={{...props.style.settings.madeby,...{marginTop:70}}}>{props.style.language.madeby} {"\n"} Kecseti István</Text>

          </ScrollView>
        </View>
      <NavBar style={props.style.settings.materialIconTextButtonsFooter} active={3} navigation={props.navigation}></NavBar>
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(InformationScreen);
