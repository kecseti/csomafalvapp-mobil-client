import React from "react";
import { View, Text, TouchableOpacity, ScrollView, SafeAreaView, Image } from "react-native";
import NavBar from "../components/NavBar";
import Header from "../components/Header";
import Icon from "react-native-vector-icons/Entypo";
import { TextField } from 'rn-material-ui-textfield';
import ProgressBar from 'react-native-progress/Bar';
import * as ImagePicker from 'expo-image-picker';
import { connect } from 'react-redux';
import Filter from 'bad-words';
import { filter } from "lodash";

class Hirdetesfeladasa extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      title:'',
      description:'',
      contact: '',
      titleError:'',
      descriptionError:'',
      contactError: '',
      uploaded: false,
      uploading: false,
      photo: ''
    }
  }

  async componentDidMount(){
    if (Platform.OS !== 'web') {
      const rights = await ImagePicker.getMediaLibraryPermissionsAsync()
      if(rights.granted === true) {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert(this.props.style.language.accesToGallery);
        }
      }
    }
  }

  pickFirstImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.8,
      base64: true
    });

    if (!result.cancelled) {
      const photo = `data:image/jpeg;base64,${result.base64}`
      this.setState({photo: photo})
    }
  };

  uploadToImgBB = async (file) => {
    const raw = new FormData();
    raw.append('image', file.split(',')[1])
    const requestOptions = {method: 'POST', body: raw };
    return fetch(`https://api.imgbb.com/1/upload?key=b4628901c2aa1af25ea4501176002f42`, requestOptions)
          .then ((response) => response.json())
  }

  onSubmit = async () =>{
    let titleError = '';
    let descriptionError = '';
    let contactError = ''; 

    if(this.state.title.length < 3) titleError = this.props.style.language.requiredField
    if(this.state.title.length > 20 ) titleError = this.props.style.language.maximum + ' 20 ' + this.props.style.language.characterRestriction

    if(this.state.description.length < 3) descriptionError = this.props.style.language.requiredField
    if(this.state.description.length > 250 ) descriptionError = this.props.style.language.maximum + ' 250 ' + this.props.style.language.characterRestriction

    if(this.state.contact.length < 3) contactError = this.props.style.language.requiredField
    if(this.state.contact.length > 40 ) contactError = this.props.style.language.maximum + ' 40 ' + this.props.style.language.characterRestriction

    const filter = new Filter();
    filter.addWords( 
      'balfasz',         'balfaszok',  'balfaszokat',  'balfaszt',
      'baszik',          'bazmeg',     'buksza',       'bukszák',
      'bukszákat',       'bukszát',    'búr',          'búrok',
      'csöcs',           'csöcsök',    'csöcsöket',    'csöcsöt',
      'fasz',            'faszfej',    'faszfejek',    'faszfejeket',
      'faszfejet',       'faszok',     'faszokat',     'faszt',
      'fing',            'fingok',     'fingokat',     'fingot',
      'franc',           'francok',    'francokat',    'francot',
      'geci',            'gecibb',     'gecik',        'geciket',
      'gecit',           'kibaszott',  'kibaszottabb', 'kúr',
      'kurafi',          'kurafik',    'kurafikat',    'kurafit',
      'kurvák',          'kurvákat',   'kurvát',       'leggecibb',
      'legkibaszottabb', 'legszarabb', 'megdöglik',    'pele',
      'pelék',           'picsa',      'picsákat',     'picsát',
      'pina',            'pinák',      'pinákat',      'pinát',
      'pofa',            'pofákat',    'pofát',        'pöcs',
      'pöcsök',          'pöcsöket',   'pöcsöt',       'punci',
      'puncik',          'segg',       'seggek',       'seggeket',
      'segget',          'seggfej',    'seggfejek',    'seggfejeket',
      'seggfejet',       'szajha',     'szajhák',      'szajhákat',
      'szajhát',         'szar',       'szarabb',      'szarik',
      'szarok',          'szarokat',   'szart', 'bazdmeg',  'bassza', 'baszik', 'kurva', 'anyád');
    
    if(filter.isProfane(this.state.description.toLowerCase())) descriptionError = this.props.style.language.noProfanity;
    if(filter.isProfane(this.state.title.toLowerCase())) titleError = this.props.style.language.noProfanity;
    if(filter.isProfane(this.state.contact.toLowerCase())) contactError = this.props.style.language.noProfanity;

    if(titleError !== '' || descriptionError !== '' || contactError !== ''){
      this.setState({titleError, descriptionError, contactError});
    } else {
      this.setState({uploading: true})

      let imageURL = '';

      if(this.state.photo !== ''){
        const resp = await this.uploadToImgBB(this.state.photo);
        imageURL = resp.data.image.url;
      }

      const headers = new Headers();
      headers.append("Content-Type", "application/json");

      const raw = JSON.stringify({title:this.state.title,description:`${this.state.description} \n ${this.state.contact}`,photoURL:imageURL});

      const requestOptions = { method: 'POST', headers: headers, body: raw, };
      
      fetch("https://csomafalapi.vercel.app/api/ads/", requestOptions)
        .then(response =>{ this.setState({titleError, descriptionError, contactError, title:'', contact:'', description:'',uploaded:true, photo: '', uploading: false}); setTimeout(() => {this.setState({uploaded: false})}, 2000) })
        .catch(err => this.setState({titleError:this.props.style.language.postingFailure, descriptionError: this.props.style.language.postingFailure, contactError: this.props.style.language.postingFailure, title:'',contact:'', description:'',uploading: false}))

    }

  }

  render(){
    return (
      <SafeAreaView style={this.props.style.postPost.container}>
        <Header style={this.props.style.postPost.materialHeader1} navigation={this.props.navigation}></Header>
        <View style={{width: '100%',height: '75%',marginTop: 10,backgroundColor:'transparent',}}>
          <ScrollView contentContainerStyle={{alignItems: 'center', paddingBottom:100}}> 
            {this.state.uploaded === true ? <Text style={this.props.style.postPost.uploaded}>{this.props.style.language.sent}</Text>: <></>}
            <Text style={this.props.style.postPost.hasznosInformaciok1}>{this.props.style.language.postPosts}</Text>

            <View style={this.props.style.postPost.rect1}>
              <TextField
                label={this.props.style.language.postTitlePlaceholder}
                value={this.state.title}
                onChangeText={ (value) => this.setState({ title: value}) }
                containerStyle={{padding: 10}}
                textColor={this.props.style.postPost.loremIpsum1.color}
                tintColor={this.props.style.postPost.buttonContainer.backgroundColor}
                characterRestriction={20}
                error={this.state.titleError}
              />
            </View>

            <View style={this.props.style.postPost.rect1}>
              <TextField
                label={this.props.style.language.postContactsPlaceholder}
                value={this.state.contact}
                onChangeText={ (value) => this.setState({ contact: value}) }
                containerStyle={{padding: 10}}
                textColor={this.props.style.postPost.loremIpsum1.color}
                tintColor={this.props.style.postPost.buttonContainer.backgroundColor}
                characterRestriction={40}
                error={this.state.contactError}
              />
            </View>

            <View style={this.props.style.postPost.rect2}>
              <TextField
                label={this.props.style.language.postContentPlaceholder}
                value={this.state.description}
                onChangeText={ (value) => this.setState({ description: value}) }
                containerStyle={{padding: 10, overflow: 'hidden', }}
                inputContainerStyle={{ marginTop:20}}
                tintColor={this.props.style.postPost.buttonContainer.backgroundColor}
                characterRestriction={250}
                textColor={this.props.style.postPost.loremIpsum1.color}
                multiline={true}
                error={this.state.descriptionError}
              />
            </View>

            <TouchableOpacity onPress={this.pickFirstImage}>
              <View style={this.props.style.postPost.rect3}>
                <View style={this.props.style.postPost.loremIpsum1Row}>
                  <Text style={this.props.style.postPost.loremIpsum1}>{this.props.style.language.postImageUpload}</Text>
                  {
                    this.state.photo === '' ?
                    <Icon name="upload" style={this.props.style.postPost.icon}></Icon>
                    :
                    <Image style={{width:75, height:50, marginLeft:10,  marginTop: -10}} source={{uri:this.state.photo}}/>
                  }
                </View>
              </View>
            </TouchableOpacity>
          
            {
              this.state.uploading === false
              ?
              <TouchableOpacity style={this.props.style.postPost.buttonContainer} onPress={this.onSubmit}>
                <Text style={this.props.style.postPost.hirdetesFeladasa}>{this.props.style.language.postSend}</Text>
                  <Icon name="publish" style={this.props.style.postPost.sendIcon}></Icon>
              </TouchableOpacity>
              :
              <ProgressBar style={{marginTop: 50}} indeterminate={true} color={this.props.style.postPost.buttonContainer.backgroundColor}/>
            }
          </ScrollView>
        </View>
        <NavBar style={this.props.style.postPost.materialIconTextButtonsFooter} navigation={this.props.navigation}></NavBar>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(Hirdetesfeladasa);

