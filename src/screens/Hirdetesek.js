import React from "react";
import { View, Image, ScrollView, ActivityIndicator, SafeAreaView, TouchableOpacity, Text } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCardWithRightButtons2 from "../components/Card";
import Header from "../components/Header";
import NavBar from "../components/NavBar";
import { connect } from 'react-redux';

import * as Network from 'expo-network';

class Hirdetesek extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards : [],
      upperLimit : 10,
      count: 9,
      errorMessage: undefined
    }
  }

  async componentDidMount(){
    const network = await Network.getNetworkStateAsync();
    if(!network.isInternetReachable){
      this.setState({errorMessage:'Nincs internetkapcsolat'})
    }
    const {result, count} = await this.fetchingData(this.state.upperLimit)
    this.setState({cards: result, count});
  }

  fetchingData = async (upperLimit) =>{
    return fetch(`https://csomafalapi.vercel.app/api/ads/${Number(upperLimit) - 10}/${upperLimit}`)
      .then(response => response.json())
      .then(result => {return {result: result.report, count: result.count}})
  }

  loadMore = async () => {
    const {result, count} = await this.fetchingData(this.state.upperLimit +10)
    const fetchedCards = result
    const oldCards = this.state.cards;
    const newCards = oldCards.concat(fetchedCards);

    this.setState({cards: newCards, upperLimit: this.state.upperLimit + 10, count });
  }

  render(){
    return (
      <SafeAreaView style={this.props.style.posts.container}>
        <Image source={require("../assets/images/home.png")} resizeMode="contain" style={this.props.style.posts.image}></Image>
        <Header style={this.props.style.posts.materialHeader1} navigation={this.props.navigation}></Header>
        <View style={this.props.style.posts.imageStack}>
          <ScrollView contentContainerStyle={{ paddingBottom: 50}}>
            {
              this.state.cards.length === 0 ?
              <ActivityIndicator style={{paddingTop: 100}} size={50} color={this.props.style.posts.moreButton.backgroundColor} /> :
              this.state.errorMessage 
              ? <Text style={{width:'100%', padding: 100, fontWeight:"bold", color:this.props.style.posts.moreButton.backgroundColor}}>{this.state.errorMessage}</Text> : 
              this.state.cards.map((data, id)=>
                <TouchableOpacity key={data._id} onPress = { () => { this.props.navigation.navigate('Hiroldala', {url: '', id: data._id, imageURL: data.photoURL, churchURL: '', title:data.title, description: data.description, date: data.toSort})}}>
                  <MaterialCardWithRightButtons2 style={this.props.style.posts.materialCardWithRightButtons2} imageURL = {data.photoURL} title = {data.title} text = {data.description} />
                </TouchableOpacity>
              )
            }
            {
              this.state.count > this.state.upperLimit ? 
              <TouchableOpacity style={this.props.style.posts.moreButton} onPress={this.loadMore}>
                <Text style={this.props.style.posts.moreButtonText}>Következő 10 betöltése</Text>
              </TouchableOpacity>
              : <></>
            }
           
          </ScrollView>
        </View>
       
        <TouchableOpacity style={this.props.style.posts.addButtonWrapper} onPress={() => this.props.navigation.navigate('Hirdetesfeladasa')}>
          <View style={{width:'100%', height:'100%',alignItems: "center"}}>
            <MaterialCommunityIconsIcon name="newspaper-plus" style={this.props.style.posts.addButtonIcon} ></MaterialCommunityIconsIcon>
          </View>
        </TouchableOpacity>
        
        <NavBar style={this.props.style.posts.materialIconTextButtonsFooter} active={1} navigation={this.props.navigation}></NavBar>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(Hirdetesek);
