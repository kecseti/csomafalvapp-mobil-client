import React, { useState, useEffect} from "react";
import { View, SafeAreaView, Text, Switch, Picker, ScrollView  } from "react-native";
import NavBar from "../components/NavBar";
import Header from "../components/Header";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { switchTheme, switchLanguage } from '../actions';
import { header } from '../../lightThemeStyles';
import AsyncStorage from '@react-native-community/async-storage';

function SettingsPage(props) {
  const [selectedValue, setSelectedValue] = useState(AsyncStorage.getItem('language') || "hu");

  useEffect(() => {
    async function fetchMyAPI() { const language = await AsyncStorage.getItem('language');  setSelectedValue(language); }
    fetchMyAPI()
  }, [])

  return (
    <SafeAreaView style={props.style.settings.container}>
        <Header style={props.style.settings.materialHeader1} navigation={props.navigation}></Header>
        <View style={{width: '100%', height: '70%'}}>
          <ScrollView contentContainerStyle={{alignItems: 'center'}}>
            <Text style={{...props.style.settings.heading,...{marginTop:70}}}>{props.style.language.settings}</Text>

            <View style={props.style.settings.rect}>
              <View style={props.style.settings.textRow}>
                <Text style={props.style.settings.text}>{props.style.language.language}</Text>
                <View  style={{ height: '100%', width: "40%"}}>
                  <Picker
                      style={{ height: '100%', width: "100%"}}
                      selectedValue={selectedValue}
                      onValueChange={async(itemValue) => { setSelectedValue(itemValue); props.switchLanguage(itemValue); await AsyncStorage.setItem('language', itemValue );}}
                    >
                      <Picker.Item label="Magyar" value="hu" />
                      <Picker.Item label="Română" value="ro" />
                    </Picker> 
                  </View>
              </View>
            </View>

            <View style={{...props.style.settings.rect,...{marginBottom:50}}}>
              <View style={props.style.settings.textRow}>
                <Text style={props.style.settings.text}>{props.style.language.theme}</Text>
                <Switch
                  value={props.style.header.container.backgroundColor !== header.container.backgroundColor}
                  thumbColor={"#FFF"}
                  trackColor={{ true: props.style.header.imageStack.backgroundColor, false: "#9E9E9E" }}
                  style={props.style.settings.switch}
                  onValueChange = {async () => {props.switchTheme();  await AsyncStorage.setItem('theme', props.style.header.container.backgroundColor !== header.container.backgroundColor ? 'light' : 'dark');}}
                ></Switch>
              </View>
            </View>

            <Text style={{...props.style.settings.madeby,...{marginTop:70}}}>{props.style.language.madeby} {"\n"} Kecseti István</Text>


          </ScrollView>
        </View>
      <NavBar style={props.style.settings.materialIconTextButtonsFooter} navigation={props.navigation}></NavBar>
    </SafeAreaView>
  );
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({ switchTheme, switchLanguage}, dispatch)
);

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
