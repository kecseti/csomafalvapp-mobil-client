import React from "react";
import { View, Image, Text,ScrollView, SafeAreaView, Linking, TouchableOpacity } from "react-native";
import ImageZoom from 'react-native-image-pan-zoom';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import NavBar from "../components/NavBar";
import Header from "../components/Header";
import { connect } from 'react-redux';

class Hiroldala extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      date: '',
      imageURL: '',
      id:'',
      churchURL: '',
      url: ''
    }
  }

  componentDidMount(){
    const date = this.props.navigation.state.params.date.split('T')[0].replace('-', '. ').replace('-', '. ');
    this.setState({url:this.props.navigation.state.params.url ,churchURL:this.props.navigation.state.params.churchURL, id:this.props.navigation.state.params.id, title: this.props.navigation.state.params.title, description:this.props.navigation.state.params.description, imageURL:this.props.navigation.state.params.imageURL, date: date})
  }

  componentDidUpdate(prevProps){
    if(this.state.id !== this.props.navigation.state.params.id){
      const date = this.props.navigation.state.params.date.split('T')[0].replace('-', '. ').replace('-', '. ');
      this.setState({url:this.props.navigation.state.params.url, churchURL:this.props.navigation.state.params.churchURL, id:this.props.navigation.state.params.id, title: this.props.navigation.state.params.title, description:this.props.navigation.state.params.description, imageURL:this.props.navigation.state.params.imageURL, date: date})
    }
  }

  render(){
    return (
      <SafeAreaView style={this.props.style.newPage.container}>
        <Header style={this.props.style.newPage.materialHeader} navigation={this.props.navigation}></Header>
        
            <Image source={{uri:this.state.imageURL}} resizeMode="contain" style={this.props.style.newPage.image2} ></Image>
            <View style={this.props.style.newPage.rect}>
              <ScrollView>
                <View style={this.props.style.newPage.titeContainer}>
                  <Text style={this.props.style.newPage.eladoVagyTudomisen}>{this.state.title}</Text>
                  <Text style={this.props.style.newPage.loremIpsum1}>{this.state.date}</Text>
                </View>
                <View style={this.props.style.newPage.image1Stack}>
                  <Image source={require("../assets/images/home.png")} resizeMode="contain" style={this.props.style.newPage.image1} ></Image>
                
                  <Text style={this.props.style.newPage.loremIpsum}>{this.state.description}</Text>
                  <Image source={{uri: this.state.churchURL}} resizeMode="contain" style={{height:350, width:'90%'}} ></Image>

                  {
                      this.state.url !== '' &&   this.state.title !== 'Miserend' ?
                      <TouchableOpacity style ={{marginTop: 10, width: '100%'}} onPress={() => Linking.openURL(this.state.url) }>
                        <View style={this.props.style.settings.rect}>
                          <View style={this.props.style.settings.textRow}>
                            <Text style={this.props.style.settings.text}>{this.props.style.language.originalNews}</Text>
                            <FontAwesomeIcon name="external-link"  style={this.props.style.settings.icon} ></FontAwesomeIcon>
                          </View>
                        </View>
                      </TouchableOpacity>
                      :
                      <></>
                  }
                </View>
              </ScrollView>
            </View>
        
        <NavBar style={this.props.style.newPage.materialIconTextButtonsFooter} navigation={this.props.navigation}></NavBar>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const { style } = state
  return { style }
};

export default connect(mapStateToProps)(Hiroldala);
